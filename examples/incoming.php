<?php

require_once __DIR__.'/client_conf.php';

class MyCoolProcessor implements \N1\Client\IncomingRequest\IAccountPaymentProcessor
{
    public function check(\N1\Xml\Request\CoreReceiver\Check $request)
    {
        $serviceName = $request->getDestination()->getName();
        $account = $request->getDestination()->getAccount();

        echo 'Check incoming request. Service: ' . $serviceName . ' Account: ' .$account . "\n";

        return new \N1\Xml\Response\Check($account, $serviceName, \N1\Xml\Response\Check::ACCOUNT_FOUND);
    }

    public function pay(\N1\Xml\Request\CoreReceiver\Response\Pay $request)
    {
        //Implement pay() method.
    }
}

$requestProcessor = (new \N1\Client\Factory($config))->getRequestProcessor(new MyCoolProcessor());

try {
    //$plainRequest = $_POST;
    $plainRequest = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<request>
  <date>2014-11-03 16:23:12</date>
  <sign>124AB73C86B7EFBBA5F3C235D3E5878DB41B47B85432E35250FA63227BC03170D4E237228138E7C84FD4A3D8EF67D2D6D1C3DECC3DA86A5E1EA111C209BD28AFBDB18FF338FE1CF1DC84FB87723010257BF704523909F2D5AECD672CC54B55458F62AEF1820BA3A1E5740FD36ADFBEC2868718554C3482E8379904604ED668E8</sign>
  <check>
    <destination>
      <account>0100000003</account>
      <name>yandex_music</name>
    </destination>
    <date>2014-11-03 16:23:12</date>
  </check>
</request>
XML;

    $plainResponse = $requestProcessor->processRequest($plainRequest);
} catch (Exception $e) {
    //process error
    $plainResponse = $requestProcessor->processException($e);
}

echo $plainResponse;
