<?php

require_once __DIR__ . '/../vendor/autoload.php';

$secretKey = '8c75ec811d1b7fd3d5559fac716e89d699f56dde4ojh0nvt87ras601d97063b8d439fbf6716dd4be3019aa855b635b';
$widgetHostUrl = 'https://pay.n1.ua/';

$signature = new \N1\Widget\Signature($secretKey);
$widget = new \N1\Widget\LinkGenerator($signature, $widgetHostUrl);

echo $widget->generatePaymentShowCaseLink('Test_Service', 12.56, 147856) . PHP_EOL;

echo $widget->generatePaymentTerminalLink(\N1\Enum\PaymentGateTypes::CREDIT_CARD, 'Test_Service', 12.56, 147856) . PHP_EOL;
