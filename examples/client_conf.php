<?php
define('ERROR_MSG','<<<client error message>>>');
define('AUTOLOAD_FILE','<<<PATH_TO/vendor/autoload.php>>>');//like::__DIR__ . '/../../../../vendor/autoload.php'

require_once AUTOLOAD_FILE;

$config = [
    'apiUrl' => 'https://processingservice.n1.ua/',
    'clientName' => '<<<INSERT_YOUR_CLIENT_NAME_HERE>>>',
    'privateKey' =>'<<<RSA
-----BEGIN RSA PRIVATE KEY-----
<<<INSERT_YOUR_PRIVATE_KEY_HERE>>>
-----END RSA PRIVATE KEY-----
RSA'
    ,
    'publicKey' =>'<<<RSA
-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCg5Jy7P7ai/6VvjUYT3yFY3DbZ
f5yYE2DdZWNVJzwW+VDQNdtM5SZ4wVjSqL1qnqW2oicFTTafZ4rTn4ecN8y/Nmmn
D3aLM2wVu/rU6sB3n3M+UlXCQt3PlSSuOwKBw5+l7UkxvGGDiGxoEBJAmBto/XEJ
KNZpK2uSVxLBWoNSwwIDAQAB
-----END PUBLIC KEY-----
RSA',
];
?>
