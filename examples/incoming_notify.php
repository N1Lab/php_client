<?php
require_once __DIR__.'/client_conf.php';

class NotifyProcessing implements \N1\Client\IncomingRequest\IOrderPaymentProcessor
{
    public function confirm(\N1\Xml\Request\CoreReceiver\Notify $request)
    {
        if ($this->client_function($request))
            return new \N1\Xml\Response\Notify();
       else
            throw new \Exception(((ERROR_MSG)?ERROR_MSG:'')); //some client error from constant ERRORMSG
    }

    public function client_function($dataObj)
    {
        /*-- example of get response values as array
        $data=[
            'status'=>$dataObj->status,
            'transaction_id'=>$dataObj->getTransaction(),
            'ext_transaction_id'=>$dataObj->ext_transaction_id,
            'abort_code'=>$dataObj->abort_code,
            'amount'=>$dataObj->getAmount(),
            'destination'=>[
                'name'=>$dataObj->getDestination()->getName(),
                'account'=>$dataObj->getDestination()->getAccount(),
            ],
            'paymentsystem_income'=>[
                'sum'=>$dataObj->getPaymentsystemIncome()->getSum(),
                'currency'=>$dataObj->getPaymentsystemIncome()->getCurrency(),
            ],
            'service_income'=>[
                'sum'=>$dataObj->getServiceIncome()->getSum(),
                'currency'=>$dataObj->getServiceIncome()->getCurrency(),
            ]
        ];
        unset($dataObj);

        return file_put_contents('./in_data/test_data.001',serialize($data));//return true or false
        */

        //** function must return  true||false */
        return true;
    }
}

 $requestProcessor = (new \N1\Client\Factory($config))->getRequestProcessor(new NotifyProcessing());

try {
    //$plainRequest = $_POST;
    $plainRequest = file_get_contents("php://input");
    $plainResponse = $requestProcessor->processRequest($plainRequest);

} catch (Exception $e) {
    //process error
    $plainResponse = $requestProcessor->processException($e);
}

echo $plainResponse;
?>
