<?php

namespace N1\Client\Tests;

use GuzzleHttp\Message\Response;
use GuzzleHttp\Stream\Stream;
use GuzzleHttp\Subscriber\Mock;
use N1\Client\OutgoingRequest\N1HttpSender;
use N1\Client\OutgoingRequest\N1OrderClientMerchant;
use N1\Signature\XmlSignature;
use N1\Xml\Response\Notify;

class ClientTest extends \PHPUnit_Framework_TestCase
{
    protected function buildN1OrderClientMerchant(array $config, $plainResponse, $statusCode = 200)
    {
        $container = new \N1\Client\Container($config);
        $signatureSerializer = $container['signatureSerializer'];
        $signatureSerializer->setSignature(
            new XmlSignature([
                'privateKey' => $config['privateKey'],
                'publicKey' => $config['publicKey'],
                'enabled' => false
            ])
        );

        $httpClient = $container['httpClient'];
        $this->setGuzzleMockResponse($httpClient, $plainResponse, $statusCode);
        $n1HttpSender = new N1HttpSender($config['apiUrl'], $signatureSerializer, $httpClient, $config['clientName']);

        return new N1OrderClientMerchant($n1HttpSender);
    }

    protected function setGuzzleMockResponse(\GuzzleHttp\Client $guzzleClient, $plainResponse, $statusCode)
    {
        $mock = new Mock([
            new Response($statusCode, [], Stream::factory($plainResponse)),
        ]);

        $guzzleClient->getEmitter()->attach($mock);
    }

    public function testMerchantMobileCreate()
    {
        $config = [
            'apiUrl' => '',
            'clientName' => '',
            'privateKey' => '',
            'publicKey' => '',
        ];

        $n1TransactionId = rand(1, 10000);

        $response = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<response>
  <status>true</status>
  <sign>4DCDD5BE...</sign>
  <date>2014-11-06 11:13:07</date>
  <transaction_id>$n1TransactionId</transaction_id>
</response>
XML;
        $n1OrderClientMerchant = $this->buildN1OrderClientMerchant($config, $response);

        /** \N1\Response\Create */
        $response = $n1OrderClientMerchant->CreateMobilePayment(
            rand(1000, 9999),
            10,
            'test@test.com',
            'Sandbox_service',
            '380987822794',
            'test@test.com',
            date('Y-m-d H:i:s')
        );

        $this->assertEquals($n1TransactionId, $response->getTransactionId());
    }

    public function testConfirm()
    {
        $config = [
            'apiUrl' => '',
            'clientName' => '',
            'privateKey' => '',
            'publicKey' => '',
        ];

        $response = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<response>
    <status>true</status>
    <sign>69C4C75524FEB19AA7611576B50E8220C60A4B00FFA0E0CDF592F0C59F447479FA7B385EA675EAF791A771F85EBA81666679701168FFB5F361F3EF5E530BFE708379BD5B4E0BD265BC44B2A3BE4CA197DFCA3CF7E0533D364D24C8D33348A973B68482FBFA5CA73BD5614C997A2A2FFAE5A64E8B164826927452735756E87C3B</sign>
    <date>2014-11-06 11:43:49</date>
</response>
XML;

        $n1OrderClientMerchant = $this->buildN1OrderClientMerchant($config, $response);

        /** \N1\Response\Confirm */
        $response = $n1OrderClientMerchant->confirmMobilePaymentByN1TransactionId(
            rand(1000, 9999),
            rand(10000, 99999)
        );

        $this->assertEquals(Notify::STATUS_SUCCESS, $response->getStatus());
    }
    /**
     * @expectedException        \N1\Errors\ApiException
     * @expectedExceptionMessage Invalid confirmation code
     */
    public function testMerchantMobileConfirmInvalidConfirmCode()
    {
        $config = [
            'apiUrl' => '',
            'clientName' => '',
            'privateKey' => '',
            'publicKey' => '',
        ];

        $response = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<response>
    <status>error</status>
    <sign>0573E66FEF...</sign>
    <error>
        <message>Invalid confirmation code</message>
        <code>13</code>
    </error>
    <date>2014-11-06 11:13:08</date>
</response>
XML;

        $n1OrderClientMerchant = $this->buildN1OrderClientMerchant($config, $response);

        $n1OrderClientMerchant->confirmMobilePaymentByN1TransactionId(
            rand(1000, 9999),
            rand(10000, 99999)
        );
    }

    /**
     * @expectedException        \N1\Errors\ApiException
     * @expectedExceptionMessage N1 service unavailable
     */
    public function testN1Unavailable()
    {
        $config = [
            'apiUrl' => '',
            'clientName' => '',
            'privateKey' => '',
            'publicKey' => '',
        ];

        $n1OrderClientMerchant = $this->buildN1OrderClientMerchant($config, '', 500);
        $n1OrderClientMerchant->confirmMobilePaymentByN1TransactionId(
            rand(1000, 9999),
            rand(10000, 99999)
        );
    }
}
