**Installation**

This library can be easily installed via composer

composer require n1/php-client

or just add it to your composer.json file directly.

**Usage**

You can find usage examples in examples/ and tests/ directories.
