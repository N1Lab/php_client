<?php

namespace N1\Client\IncomingRequest;

use N1\Xml\Request\CoreReceiver\Notify;

interface IOrderPaymentProcessor
{
    /**
     * @param Notify $request
     * @return \N1\Xml\Response\Notify
     */
    public function confirm(Notify $request);
}
