<?php

namespace N1\Client\IncomingRequest;

use JMS\Serializer\SerializerInterface;
use N1\Errors\ApiErrorCodes;
use N1\Errors\ApiException;
use N1\Xml\Request\Request;

class RequestProcessor
{
    protected $processor;
    protected $serializer;

    public function __construct($processor, SerializerInterface $serializer)
    {
        $this->processor = $processor;

        if (!($this->isProcessorImplementsInterface('N1\Client\IncomingRequest\IAccountPaymentProcessor') ||
            $this->isProcessorImplementsInterface('N1\Client\IncomingRequest\IOrderPaymentProcessor'))
        )
        {
            throw new \Exception('Processor must implements at list one interface: IAccountPaymentProcessor or IOrderPaymentProcessor');
        }

        $this->serializer = $serializer;
    }

    protected function isProcessorImplementsInterface($interface)
    {
        return in_array($interface, class_implements($this->processor));
    }

    /**
     * @return Request
     */
    protected function parseRequest($plainRequest)
    {
        return $this->serializer->deserialize(
            $plainRequest,
            'N1\Xml\Request\CoreReceiver\Request',
            'xml'
        );
    }

    public function processRequest($plainRequest)
    {
        $request = $this->parseRequest($plainRequest);

        $request_type_parameters = [
            'check' => [
                'interface' => 'N1\Client\IncomingRequest\IAccountPaymentProcessor',
                'processor_method' => 'check',
                'request_parameter' => 'check',
                'response_instance' => '\N1\Xml\Response\Check',
            ],
            'pay' => [
                'interface' => 'N1\Client\IncomingRequest\IAccountPaymentProcessor',
                'processor_method' => 'pay',
                'request_parameter' => 'pay',
                'response_instance' => '\N1\Xml\Response\Pay',
            ],
            'notify' => [
                'interface' => 'N1\Client\IncomingRequest\IOrderPaymentProcessor',
                'processor_method' => 'confirm',
                'request_parameter' => 'notify',
                'response_instance' => '\N1\Xml\Response\Notify',
            ]
        ];

        if (!array_key_exists($request->getType(), $request_type_parameters)) {
            throw new \Exception("Can't process request with type " . $request->getType());
        }

        if (!$this->isProcessorImplementsInterface($request_type_parameters[$request->getType()]['interface'])) {
            throw new \Exception(
                sprintf('Processor must implements interface "%s" to process "%s" request',
                    [
                        $request_type_parameters[$request->getType()]['interface'],
                        $request->getType(),
                    ]
                )
            );
        }

        $request_data = $request->{$request_type_parameters[$request->getType()]['request_parameter']};

        $data = call_user_func([$this->processor, $request_type_parameters[$request->getType()]['processor_method']], $request_data);

        if (!$data instanceof $request_type_parameters[$request->getType()]['response_instance']) {
            throw new \Exception('Processor pay response must be instance of ' . $request_type_parameters[$request->getType()]['response_instance']);
        }

        return $this->serializer->serialize($data, 'xml');
    }

    public function processException(\Exception $e)
    {
        $code = ($e instanceof ApiException) ? $e->getCode() : ApiErrorCodes::SYSTEM_ERROR;
        $parameters = ($e instanceof ApiException) ? $e->getParameters() : [];

        return $this->serializer->serialize(
            new \N1\Xml\Response\Error(
                new \N1\Xml\Response\ErrorException($code, $e->getMessage(), $parameters)
            ), 'xml')
       ;
    }
}
