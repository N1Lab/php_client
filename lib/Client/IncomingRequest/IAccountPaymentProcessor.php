<?php

namespace N1\Client\IncomingRequest;

use N1\Xml\Request\CoreReceiver\Check;
use N1\Xml\Request\CoreReceiver\Response\Pay;

interface IAccountPaymentProcessor
{
    /**
     * @param Check $request
     * @return \N1\Xml\Response\Check
     */
    public function check(Check $request);

    /**
     * @param Pay $request
     * @return \N1\Xml\Response\Pay
     */
    public function pay(Pay $request);
}
