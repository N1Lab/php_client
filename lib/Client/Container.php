<?php

namespace N1\Client;

use Doctrine\Common\Annotations\AnnotationRegistry;
use GuzzleHttp\Client;
use JMS\Serializer\SerializerBuilder;
use N1\Client\OutgoingRequest\N1HttpSender;
use N1\Client\OutgoingRequest\N1OrderClientMerchant;
use N1\Signature\XmlSignature;
use N1\Xml\SignatureSerializer;
use Pimple\Container as PimpleContainer;

class Container extends PimpleContainer
{
    public function __construct(array $config)
    {
        $this['jms_serializer'] = function () {
            $loader = require '/../'.AUTOLOAD_FILE;

            AnnotationRegistry::registerLoader([$loader, 'loadClass']);

            $builder = SerializerBuilder::create();

            return $builder->build();
        };

        $this['signatureSerializer'] = function ($c) use ($config) {
            $signatureSerializer = new SignatureSerializer($c['jms_serializer']);
            $signatureSerializer->setSignature(
                new XmlSignature([
                    'privateKey' => $config['privateKey'],
                    'publicKey' => $config['publicKey'],
                ])
            );
            return $signatureSerializer;
        };

        $this['httpClient'] = function () {
            return new Client();
        };

        $this['n1HttpSender'] = function ($c) use ($config) {

            return new N1HttpSender(
                $config['apiUrl'],
                $c['signatureSerializer'],
                $c['httpClient'],
                $config['clientName']
            );
        };

        $this['n1OrderClientMerchant'] = function ($c) {
            return new N1OrderClientMerchant($c['n1HttpSender']);
        };
    }
}
