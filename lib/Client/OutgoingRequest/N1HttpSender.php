<?php

namespace N1\Client\OutgoingRequest;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use JMS\Serializer\SerializerInterface;
use N1\Errors\ApiErrorCodes;
use N1\Errors\ApiException;
use N1\Xml\Request\Confirm\Confirm;
use N1\Xml\Request\Check\Check;
use N1\Xml\Request\Create\Create;
use N1\Xml\Request\Pay\Pay;
use N1\Xml\Request\Request;
use N1\Xml\Request\RequestInterface;
use N1\Xml\Request\Status\Status;

class N1HttpSender
{
    private $apiUrl;
    private $serializer;
    private $httpClient;
    private $clientName;

    public function __construct($apiUrl, SerializerInterface $serializer, ClientInterface $httpClient, $clientName)
    {
        $this->apiUrl = $apiUrl;
        $this->serializer = $serializer;
        $this->httpClient = $httpClient;
        $this->clientName = $clientName;
    }

    protected function sendRequest(RequestInterface $data, $responseType)
    {
        $xmlData = $this->serializer->serialize(new Request($data), 'xml');

        try {
            $response = $this->httpClient->post($this->apiUrl . $this->clientName, ['body' => $xmlData]);
        } catch (RequestException $e) {
            throw new ApiException(ApiErrorCodes::SERVICE_UNAVAILABLE, 'N1 service unavailable');
        }

        $result = $this->serializer->deserialize($response->getBody()->getContents(), $responseType, 'xml');

        if ($result->getStatus() == 'error') {
            throw new ApiException(
                $result->error->getCode(),
                $result->error->getMessage(),
                $result->error->getParameters()
            );
        }

        return $result;
    }

    public function sendCheckRequest(Check $data)
    {
        return $this->sendRequest($data, 'N1\Xml\Response\Check');
    }

    /**
     * @param Pay $data
     * @return \N1\Xml\Response\Pay
     * @throws ApiException
     */
    public function sendPayRequest(Pay $data)
    {
        return $this->sendRequest($data, 'N1\Xml\Response\Pay');
    }

    /**
     * @param Create $data
     * @return \N1\Xml\Response\Pay
     * @throws ApiException
     */
    public function sendCreateRequest(Create $data)
    {
        return $this->sendRequest($data, 'N1\Xml\Response\Create');
    }

    /**
     * @param Confirm $data
     * @return \N1\Xml\Response\Confirm
     * @throws ApiException
     */
    public function sendConfirmRequest(Confirm $data)
    {
        return $this->sendRequest($data, 'N1\Xml\Response\Confirm');
    }

    /**
     * @param Status $data
     * @return \N1\Xml\Response\Status
     * @throws ApiException
     */
    public function sendStatusRequest(Status $data)
    {
        return $this->sendRequest($data, 'N1\Xml\Response\Status');
    }
}
