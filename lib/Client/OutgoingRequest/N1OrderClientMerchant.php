<?php

namespace N1\Client\OutgoingRequest;

use N1\Xml\Request\CheckCreate\Destination;
use N1\Xml\Request\CheckCreate\Source;
use N1\Xml\Request\Confirm\InternalTransactionConfirm;
use N1\Xml\Request\Create\Create;
use N1\Xml\Request\Status\InternalTransactionStatus;
use N1\Xml\Request\Status\ExternalTransactionStatusByOrder;

class N1OrderClientMerchant
{
    private $sender;

    public function __construct(N1HttpSender $sender)
    {
        $this->sender = $sender;
    }

    public function createCardPayment(
        $merchantTransactionId,
        $amount,
        $account,
        $service,
        $phoneNumber,
        $email,
        $paymentGate,
        $redirectUrl,
        $extDate,
        $description
    ) {
        return $this->sender->sendCreateRequest(
            (new Create(
                $amount,
                new Destination($account, $service),
                new Source($paymentGate),
                [
                    'PhoneNumber' => $phoneNumber,
                    'Email' => $email,
                    'redirectUrl' => $redirectUrl,
                    'description' => $description,
                ]
            ))
            ->setExternalId($merchantTransactionId)
            ->setExternalDate($extDate)
        );
    }
        
    public function createMobilePayment($merchantTransactionId, $amount, $account, $service, $phoneNumber, $email, $extDate)
    {
        return $this->sender->sendCreateRequest(
            (new Create(
                $amount,
                new Destination($account, $service),
                new Source('ksmobilecommerce'),
                [
                    'PhoneNumber' => $phoneNumber,
                    'Email' => $email,
                ]
            ))
            ->setExternalId($merchantTransactionId)
            ->setExternalDate($extDate)
        );
    }

    public function createEMoneyPayment($merchantTransactionId, $amount, $account, $service, $phoneNumber, $email, $extDate, $paymentGateName,
                                        $payerPrepaidId, $comment)
    {
        return $this->sender->sendCreateRequest(
            (new Create(
                $amount,
                new Destination($account, $service),
                new Source($paymentGateName),
                [
                    'PhoneNumber' => $phoneNumber,
                    'Email' => $email,
                    'payerPrepaidId' => $payerPrepaidId,
                    'comment' => $comment,
                ]
            ))
                ->setExternalId($merchantTransactionId)
                ->setExternalDate($extDate)
        );
    }

    public function createN1ButtonPayment($merchantTransactionId, $amount, $account, $service, $phoneNumber, $email, $extDate)
    {
        return $this->sender->sendCreateRequest(
            (new Create(
                $amount,
                new Destination($account, $service),
                new Source('n1_pg'),
                [
                    'PhoneNumber' => $phoneNumber,
                    'Email' => $email,
                ]
            ))
            ->setExternalId($merchantTransactionId)
            ->setExternalDate($extDate)
        );
    }

    public function confirmMobilePaymentByN1TransactionId($transactionId, $confirmCode)
    {
        return $this->sender->sendConfirmRequest(
            new InternalTransactionConfirm(
                $transactionId,
                [
                    'ConfirmCode' => $confirmCode
                ]
            )
        );
    }

    public function confirmEMoneyPaymentByN1TransactionId($transactionId, $confirmCode)
    {
        return $this->sender->sendConfirmRequest(
            new InternalTransactionConfirm(
                $transactionId,
                [
                    'ConfirmCode' => $confirmCode
                ]
            )
        );
    }

    public function getStatusByN1TransactionId($transactionId)
    {
        return $this->sender->sendStatusRequest(
            new InternalTransactionStatus($transactionId)
        );
    }
    
    public function GetStatusByOrder($account_id, $service_name, $order_id)
    {
        return $this->sender->sendStatusRequest(new ExternalTransactionStatusByOrder(
                                                        $order_id,
                                                        new Destination($account_id, $service_name))
        );
    }
}
