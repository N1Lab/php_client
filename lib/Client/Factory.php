<?php

namespace N1\Client;

use N1\Client\IncomingRequest\RequestProcessor;

class Factory
{
    public function __construct(array $config)
    {
        $this->container = new Container($config);
    }

    /**
     * @return \N1\Client\OutgoingRequest\N1OrderClientMerchant
     */
    public function getOrderClientMerchant()
    {
        return $this->container['n1OrderClientMerchant'];
    }

    public function getRequestProcessor($processor)
    {
        return new RequestProcessor($processor, $this->container['signatureSerializer']);
    }
}
