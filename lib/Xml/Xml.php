<?php

namespace N1\Xml;

use JMS\Serializer\Annotation\PostDeserialize;
use N1\Errors\ApiErrorCodes;
use N1\Errors\ApiException;
use Symfony\Component\Validator\Validation as SymfonyValidation;

class Xml
{
    /**
     * @PostDeserialize
     */
    public function validate()
    {
        $validator = SymfonyValidation::createValidatorBuilder()
            ->enableAnnotationMapping()
            ->getValidator();

        $violations = $validator->validate($this);

        if ($violations->count() == 0) {
            return true;
        }

        throw new ApiException(ApiErrorCodes::INVALID_REQUEST, $violations->get(0)->getMessage());
    }
}
