<?php

namespace N1\Xml\Response;

use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\XmlRoot;

/**
 * Class Error
 * @package N1\Xml\Response
 * @XmlRoot("response")
 */
class Status extends Response
{
    /** @Type("N1\Xml\Response\Transaction") */
    private $status_transaction;

    public function __construct(Transaction $status_transaction)
    {
        $this->status_transaction = $status_transaction;
        parent::__construct();
    }

    /**
     * @return Transaction
     */
    public function getStatusTransaction()
    {
        return $this->status_transaction;
    }
}
