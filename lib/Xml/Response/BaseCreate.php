<?php

namespace N1\Xml\Response;

use JMS\Serializer\Annotation\Type;

class BaseCreate extends Response
{
    /** @Type("N1\Xml\Response\Redirect") */
    protected $redirect;

    /**
     * @param mixed $redirect
     */
    public function setRedirect(Redirect $redirect)
    {
        $this->redirect = $redirect;
    }

    /**
     * @return \N1\Xml\Response\Redirect
     */
    public function getRedirect()
    {
        return $this->redirect;
    }
}
