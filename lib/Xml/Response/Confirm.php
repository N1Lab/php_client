<?php

namespace N1\Xml\Response;

use JMS\Serializer\Annotation\XmlRoot;

/**
 * @XmlRoot("response")
 */
class Confirm extends Response
{
    use \N1\Xml\Amount\AmountTrait;
}
