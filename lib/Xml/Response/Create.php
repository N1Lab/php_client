<?php

namespace N1\Xml\Response;

use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\XmlRoot;

/**
 * @XmlRoot("response")
 */
class Create extends BaseCreate
{
    /**
     * @Type("integer")
     */
    protected $transaction_id;

    public function __construct($transaction_id, $parameters = [])
    {
        $this->transaction_id = $transaction_id;
        $this->setParameters($parameters);
        parent::__construct();
    }

    /**
     * @return mixed
     */
    public function getTransactionId()
    {
        return $this->transaction_id;
    }
}
