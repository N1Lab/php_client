<?php

namespace N1\Xml\Response;

use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\XmlRoot;

/**
 * Class Error
 * @package N1\Xml\Response
 * @XmlRoot("response")
 */
class Error extends Response
{
    /**
     * @Type("N1\Xml\Response\ErrorException")
     */
    public $error;

    public function __construct(ErrorException $error)
    {
        $this->setStatus('error');
        $this->error = $error;
        parent::__construct();
    }
}
