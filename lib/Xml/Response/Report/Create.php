<?php

namespace N1\Xml\Response\Report;

use N1\Xml\Response\Response;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\XmlRoot;

/** @XmlRoot("response") */
class Create extends Response
{
    /**
     * @Type("integer")
     */
    public $id;

    public function __construct($id, $message = "")
    {
        $this->setStatus('true');
        $this->id = $id;
    }
}
