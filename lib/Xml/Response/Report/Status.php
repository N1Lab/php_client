<?php

namespace N1\Xml\Response\Report;

use N1\Xml\Response\Response;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\XmlRoot;

/** @XmlRoot("response") */
class Status extends Response
{
    /**
     * @Type("integer")
     */
    public $count;

    /**
     * @Type("boolean")
     */
    public $created = false;

    public function __construct($count, $created)
    {
        $this->setStatus('true');
        $this->count = $count;
        $this->created = $created;
    }
}
