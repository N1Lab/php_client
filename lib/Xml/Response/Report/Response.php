<?php

namespace N1\Xml\Response\Report;

use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\XmlRoot;
use JMS\Serializer\Annotation\XmlElement;

/** @XmlRoot("response") */
class Response extends \N1\Xml\Response\Response
{
    /**
     * @XmlElement(cdata=false)
     * @Type("string")
     */
    public $data;

    public function __construct($data)
    {
        $this->setStatus('true');
        $this->data = json_encode($data);
    }
}
