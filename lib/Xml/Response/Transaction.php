<?php

namespace N1\Xml\Response;

use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\XmlElement;
use JMS\Serializer\Annotation\XmlRoot;

/**
 * Class Transaction
 * @package N1\Xml\Response
 * @XmlRoot("status_transaction")
 */
class Transaction
{
    /**
     * @Type("string")
     * @XmlElement(cdata=false)
     */
    public $internal_id;
    /**
     * @Type("string")
     * @XmlElement(cdata=false)
     */
    public $created;
    /**
     * @Type("string")
     * @XmlElement(cdata=false)
     */
    public $last_update;
    /**
     * @Type("string")
     * @XmlElement(cdata=false)
     */
    public $step;
    /**
     * @Type("string")
     * @XmlElement(cdata=false)
     */
    public $status;
    /**
     * @Type("string")
     * @XmlElement(cdata=false)
     */
    public $service_id;
    /**
     * @Type("string")
     * @XmlElement(cdata=false)
     */
    public $service_confirmation_date;
    /**
     * @Type("string")
     * @XmlElement(cdata=false)
     */
    public $service_account;
    /**
     * @Type("string")
     * @XmlElement(cdata=false)
     */
    public $service_amount;
    /**
     * @Type("string")
     * @XmlElement(cdata=false)
     */
    public $service_income;
    /**
     * @Type("string")
     * @XmlElement(cdata=false)
     */
    public $service_discount;
    /**
     * @Type("string")
     * @XmlElement(cdata=false)
     */
    public $payment_gate_id;
    /**
     * @Type("string")
     * @XmlElement(cdata=false)
     */
    public $payment_gate_confirmation_date;
    /**
     * @Type("string")
     * @XmlElement(cdata=false)
     */
    public $payment_gate_external_id;
    /**
     * @Type("string")
     * @XmlElement(cdata=false)
     */
    public $payment_gate_income;
    /**
     * @Type("string")
     * @XmlElement(cdata=false)
     */
    public $payment_gate_commission;
    /**
     * @Type("string")
     * @XmlElement(cdata=false)
     */
    public $service_payout_id;
    /**
     * @Type("string")
     * @XmlElement(cdata=false)
     */
    public $node_id;
    /**
     * @Type("string")
     * @XmlElement(cdata=false)
     */
    public $service_income_with_rate;
    /**
     * @Type("string")
     * @XmlElement(cdata=false)
     */
    public $n1_commission;
    /**
     * @Type("string")
     * @XmlElement(cdata=false)
     */
    public $service_amount_discount;
    /**
     * @Type("string")
     * @XmlElement(cdata=false)
     */
    public $initiator;
    /**
     * @Type("string")
     * @XmlElement(cdata=false)
     */
    public $additional_parameters;
    /**
     * @Type("string")
     * @XmlElement(cdata=false)
     */
    public $service_external_id;
    /**
     * @Type("string")
     * @XmlElement(cdata=false)
     */
    public $service_label;

    /**
     * @Type("string")
     * @XmlElement(cdata=false)
     */
    public $payment_gate_label;

    /**
     * @Type("string")
     * @XmlElement(cdata=false)
     */
    public $payment_gate_service_label;
}
