<?php

namespace N1\Xml\Response;

use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\XmlElement;
use JMS\Serializer\Annotation\XmlRoot;
use N1\Xml\Request\Traits\Parameters;

/**
 * @XmlRoot("error")
 */
class ErrorException
{
    use Parameters;
    /**
     * @Type("string")
     * @XmlElement(cdata=false)
     */
    protected $message;

    /**
     * @Type("integer")
     */
    protected $code;

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    public function __construct($code, $message, $parameters = [])
    {
        $this->code = $code;
        $this->message = $message;
        $this->setParameters($parameters);
    }
}
