<?php

namespace N1\Xml\Response;

use JMS\Serializer\Annotation\Type;
use N1\Xml\Annotation\NotBlank;
use N1\Xml\Request\Traits\Parameters;
use JMS\Serializer\Annotation\XmlElement;

class Response
{
    const STATUS_SUCCESS = 'true';

    use Parameters;

    /**
     * @Type("string")
     * @XmlElement(cdata=false)
     */
    protected $status = 'true';

    /**
     * @Type("string")
     * @XmlElement(cdata=false)
     */
    public $sign;

    /**
     * @Type("N1\Xml\Response\ErrorException")
     */
    public $error;

    /**
     * @NotBlank
     * @XmlElement(cdata=false)
     * @Type("DateTime<'Y-m-d H:i:s', 'Europe/Kiev'>")
     */
    public $date;

    public function __construct()
    {
        $this->date = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    public function isSuccess()
    {
        return $this->getStatus() == self::STATUS_SUCCESS;
    }
}
