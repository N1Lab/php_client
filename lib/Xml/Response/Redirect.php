<?php

namespace N1\Xml\Response;

use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\XmlElement;
use JMS\Serializer\Annotation\XmlRoot;
use N1\Xml\Request\Traits\Parameters;

/**
 * @XmlRoot("redirect")
 */
class Redirect
{
    use Parameters;

    const POST = 'POST';

    /**
     * @Type("string")
     * @XmlElement(cdata=false)
     */
    private $url;

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }
    /**
     * @Type("string")
     * @XmlElement(cdata=false)
     */
    private $type;

    public function __construct($url, $type = 'POST', $postParameters = [])
    {
        $this->type = $type;
        $this->url = $url;
        $this->setParameters($postParameters);
    }
}
