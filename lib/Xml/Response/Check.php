<?php

namespace N1\Xml\Response;

use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\XmlElement;
use JMS\Serializer\Annotation\XmlRoot;

/**
 * @XmlRoot("response")
 */
class Check extends Response
{
    const ACCOUNT_FOUND = 'true';
    const ACCOUNT_NOT_FOUND = 'false';

    /**
     * @Type("string")
     * @XmlElement(cdata=false)
     */
    protected $account;

    /**
     * @Type("string")
     * @XmlElement(cdata=false)
     */
    protected $service;

    public function __construct($account, $service, $status, $parameters = [])
    {
        $this->account = $account;
        $this->service = $service;
        $this->status = $status;
        $this->setParameters($parameters);
        parent::__construct();
    }

    /**
     * @return mixed
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @return mixed
     */
    public function getService()
    {
        return $this->service;
    }
}
