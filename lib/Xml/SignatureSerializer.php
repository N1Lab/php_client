<?php

namespace N1\Xml;

use JMS\Serializer\DeserializationContext;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\Serializer;
use JMS\Serializer\SerializerInterface;
use N1\Signature\XmlSignature;

class SignatureSerializer implements SerializerInterface
{
    private $serializer;
    private $signature;

    public function __construct(Serializer $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @param XmlSignature $signature
     */
    public function setSignature(XmlSignature $signature)
    {
        $this->signature = $signature;
    }


    /**
     * Serializes the given data to the specified output format.
     *
     * @param object $data
     * @param string $format
     * @param SerializationContext $context
     *
     * @return string
     */
    public function serialize($data, $format, SerializationContext $context = null)
    {
        if ($this->signature instanceof XmlSignature && $this->signature->isEnabled() && $format == 'xml') {
            $signTagName = $this->signature->getTagName();
            $data->{$signTagName} = '';
        }

        $data = $this->serializer->serialize($data, $format, $context);

        if ($this->signature instanceof XmlSignature && $this->signature->isEnabled() && $format == 'xml') {
            $data = $this->signature->signData($data);
        }

        return $data;
    }

    /**
     * Deserializes the given data to the specified type.
     *
     * @param string $data
     * @param string $type
     * @param string $format
     * @param DeserializationContext $context
     *
     * @return object
     */
    public function deserialize($data, $type, $format, DeserializationContext $context = null)
    {
        if ($this->signature instanceof XmlSignature && $this->signature->isEnabled() && $format == 'xml') {
            $this->signature->checkSign($data);
        }

        return $this->serializer->deserialize($data, $type, $format, $context);
    }
}
