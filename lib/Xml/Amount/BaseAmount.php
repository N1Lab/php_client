<?php

namespace N1\Xml\Amount;

use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\XmlElement;

abstract class BaseAmount
{
    /** @Type("double") */
    protected $sum;

    /**
     * @return mixed
     */
    public function getSum()
    {
        return $this->sum;
    }

    /**
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @XmlElement(cdata=false) 
     * @Type("string") 
     */
    public $currency;

    public function __construct($sum, $currency)
    {
        $this->sum = $sum;
        $this->currency = $currency;
    }
}
