<?php

namespace N1\Xml\Amount;

use JMS\Serializer\Annotation\Type;

trait AmountTrait
{
    /** @Type("double") */
    protected $amount;
    
    /** @Type("N1\Xml\Amount\PaymentSystemIncome") */
    protected $paymentsystem_income;

    /** @Type("N1\Xml\Amount\ServiceIncome") */
    protected $service_income;

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     * @return $this
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * @return PaymentSystemIncome
     */
    public function getPaymentsystemIncome()
    {
        return $this->paymentsystem_income;
    }

    /**
     * @param mixed $paymentsystem_income
     * @return $this
     */
    public function setPaymentsystemIncome(PaymentSystemIncome $paymentsystem_income)
    {
        $this->paymentsystem_income = $paymentsystem_income;

        return $this;
    }

    /**
     * @return ServiceIncome
     */
    public function getServiceIncome()
    {
        return $this->service_income;
    }

    /**
     * @param mixed $service_income
     * @return $this
     */
    public function setServiceIncome(ServiceIncome $service_income)
    {
        $this->service_income = $service_income;

        return $this;
    }
}
