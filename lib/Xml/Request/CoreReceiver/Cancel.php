<?php

namespace N1\Xml\Request\CoreReceiver;

use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\XmlElement;
use JMS\Serializer\Annotation\XmlRoot;
use N1\Xml\Request\CheckCreate\Destination;
use N1\Xml\Request\Traits\Parameters;

/** @XmlRoot("notify") */
class Cancel implements ICoreReceiverRequest
{
    use Parameters;

    /** @Type("integer") */
    public $transaction;

    /**
     * @Type("string")
     * @XmlElement(cdata=false)
     */
    public $extTransaction;

    /**
     * @Type("N1\Xml\Request\CheckCreate\Destination")
     */
    private $destination;

    /**
     * @return Destination
     */
    public function getDestination()
    {
        return $this->destination;
    }

    public function __construct($transaction, $destination, $parameters = [])
    {
        $this->transaction = $transaction;
        $this->destination = $destination;
        $this->setParameters($parameters);
    }
}
