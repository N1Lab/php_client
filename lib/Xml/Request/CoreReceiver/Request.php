<?php

namespace N1\Xml\Request\CoreReceiver;

use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\XmlRoot;
use JMS\Serializer\Annotation\Exclude;
use JMS\Serializer\Annotation\PostDeserialize;
use JMS\Serializer\Annotation\XmlElement;
use N1\Errors\ApiErrorCodes;
use N1\Errors\ApiException;
use N1\Xml\Request\BaseRequest;

/**
 * @XmlRoot("request")
 */
class Request extends BaseRequest
{
    /**
     * @Exclude
     */
    public $type;

    /**
     * @Type("N1\Xml\Request\CoreReceiver\Check")
     */
    public $check;

    /**
     * @var Create
     * @Type("N1\Xml\Request\CoreReceiver\Create")
     */
    public $create;

    /**
     * @var Pay
     * @Type("N1\Xml\Request\CoreReceiver\Pay")
     */
    public $pay;

    /**
     * @Type("N1\Xml\Request\CoreReceiver\Notify")
     */
    public $notify;

    /**
     * @Type("string")
     * @XmlElement(cdata=false)
     */
    public $sign;

    public function __construct(ICoreReceiverRequest $type)
    {
        parent::__construct();

        if ($type instanceof Pay) {
            $this->pay = $type;
            $this->type = 'pay';
        } elseif ($type instanceof Create) {
            $this->create = $type;
            $this->type = 'create';
        } elseif ($type instanceof Check) {
            $this->check = $type;
            $this->type = 'check';
        } elseif ($type instanceof Notify) {
            $this->notify = $type;
            $this->type = 'notify';
        } else {
            throw new ApiException(ApiErrorCodes::INVALID_REQUEST);
        }
    }

    /**
     * Возвращает тип запроса(check, create, confirm или status)
     *
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @PostDeserialize
     */
    public function setRequestType()
    {
        $requestTypes = ['check', 'create', 'pay', 'notify'];

        foreach ($requestTypes as $type) {
            if ($this->{$type} instanceof ICoreReceiverRequest) {
                $this->type = $type;

                return true;
            }
        }

        throw new ApiException(ApiErrorCodes::INVALID_REQUEST);
    }
}
