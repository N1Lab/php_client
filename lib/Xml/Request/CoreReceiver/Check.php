<?php

namespace N1\Xml\Request\CoreReceiver;

use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\XmlElement;
use JMS\Serializer\Annotation\XmlRoot;
use N1\Xml\Request\CheckCreate\Destination;
use N1\Xml\Request\Traits\Parameters;
use N1\Xml\Xml;

/**
 * @XmlRoot("check")
 */
class Check extends Xml implements ICoreReceiverRequest
{
    use Parameters;

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @return Destination
     */
    public function getDestination()
    {
        return $this->destination;
    }
    /**
     * @Type("N1\Xml\Request\CheckCreate\Destination")
     */
    protected $destination;

    /**
     * @XmlElement(cdata=false)
     * @Type("DateTime<'Y-m-d H:i:s'>")
     */
    protected $date;

    public function __construct(Destination $destination, $parameters = [])
    {
        $this->destination = $destination;
        $this->date = new \DateTime();
        $this->setParameters($parameters);
    }
}
