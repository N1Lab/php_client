<?php

namespace N1\Xml\Request\CoreReceiver;

use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\XmlElement;
use JMS\Serializer\Annotation\XmlRoot;
use N1\Xml\Request\Traits\Parameters;
use N1\Xml\Amount\PaymentSystemIncome;
use N1\Xml\Amount\ServiceIncome;

/** @XmlRoot("notify") */
class Notify implements ICoreReceiverRequest
{
    const STATUS_SUCCESS = 'true';
    const STATUS_ABORT = 'abort';

    const ABORT_CONFIRMATION_TIMEOUT = 1;

    use Parameters;
    use \N1\Xml\Amount\AmountTrait;

    /**
     * @Type("string")
     * @XmlElement(cdata=false)
     */
    public $status;

    /** @Type("integer") */
    private $transaction_id;

    /**
     * @Type("string")
     * @XmlElement(cdata=false)
     */
    public $ext_transaction_id;

    /**
     * @Type("N1\Xml\Request\CheckCreate\Destination")
     */
    private $destination;

    /** @Type("integer") */
    public $abort_code;

    public function __construct(
        $status,
        $transactionId,
        $extTransactionId,
        $amount,
        PaymentSystemIncome $paymentsystemIncome,
        ServiceIncome $serviceIncome,
        $destination,
        $parameters = [],
        $abort_code = null
    ) {
        $this->status = $status;
        $this->transaction_id = $transactionId;
        $this->ext_transaction_id = $extTransactionId;
        $this->amount = $amount;
        $this->destination = $destination;
        $this->abort_code = $abort_code;
        $this->setPaymentsystemIncome($paymentsystemIncome);
        $this->setServiceIncome($serviceIncome);
        $this->setParameters($parameters);
    }

    /**
     * @return mixed
     */
    public function getTransaction()
    {
        return $this->transaction_id;
    }

    public function getDestination()
    {
        return $this->destination;
    }
}
