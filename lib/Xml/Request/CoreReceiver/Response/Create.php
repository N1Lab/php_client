<?php

namespace N1\Xml\Request\CoreReceiver\Response;

use N1\Xml\Response\BaseCreate;

class Create extends BaseCreate
{
    protected $extTransactionId;

    /**
     * @return null
     */
    public function getExtTransactionId()
    {
        return $this->extTransactionId;
    }

    public function __construct($transaction, $extTransactionId = null, $parameters = [])
    {
        $this->transaction = $transaction;
        $this->extTransactionId = $extTransactionId;
        $this->setParameters($parameters);
        parent::__construct();
    }
}
