<?php

namespace N1\Xml\Request\CoreReceiver\Response;

use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\XmlRoot;
use N1\Xml\Response\BaseCreate;

/**
 * @XmlRoot("response")
 */
class Pay extends BaseCreate
{
    /**
     * @Type("integer")
     */
    protected $ext_transaction_id;

    public function __construct($transaction_id, $parameters = [])
    {
        $this->ext_transaction_id = $transaction_id;
        $this->setParameters($parameters);
        parent::__construct();
    }

    /**
     * @return mixed
     */
    public function getTransactionId()
    {
        return $this->ext_transaction_id;
    }
}
