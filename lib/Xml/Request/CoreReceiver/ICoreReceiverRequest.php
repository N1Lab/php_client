<?php

namespace N1\Xml\Request\CoreReceiver;

interface ICoreReceiverRequest
{
    /** @return \N1\Xml\Request\CheckCreate\Destination */
    public function getDestination();
}
