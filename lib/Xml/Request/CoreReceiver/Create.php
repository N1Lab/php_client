<?php

namespace N1\Xml\Request\CoreReceiver;

use JMS\Serializer\Annotation\Type;
use N1\Xml\Request\CheckCreate\Destination;
use N1\Xml\Request\Traits\Parameters;
use N1\Xml\Amount\PaymentSystemIncome;
use N1\Xml\Amount\ServiceIncome;

class Create implements ICoreReceiverRequest
{
    use Parameters;

    use \N1\Xml\Amount\AmountTrait;

    /** @Type("integer") */
    protected $transaction_id;

    /**
     * @return mixed
     */
    public function getTransactionId()
    {
        return $this->transaction_id;
    }

    /**
     * @return Destination
     */
    public function getDestination()
    {
        return $this->destination;
    }

    /**
     * @Type("N1\Xml\Request\CheckCreate\Destination")
     */
    protected $destination;

    public function __construct(
        $transaction,
        $amount,
        PaymentSystemIncome $paymentsystemIncome,
        ServiceIncome $serviceIncome,
        Destination $destination,
        $parameters = []
    )
    {
        $this->transaction_id = $transaction;
        $this->amount = $amount;
        $this->setPaymentsystemIncome($paymentsystemIncome);
        $this->setServiceIncome($serviceIncome);
        $this->destination = $destination;
        $this->setParameters($parameters);
    }
}
