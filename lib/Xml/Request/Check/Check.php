<?php

namespace N1\Xml\Request\Check;

use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\XmlRoot;
use N1\Xml\Annotation\NotBlank;
use N1\Xml\Request\CheckCreate\Destination;
use N1\Xml\Request\CheckCreate\Source;
use N1\Xml\Request\RequestInterface;
use N1\Xml\Request\Traits\Parameters;
use N1\Xml\Xml;

/**
 * @XmlRoot("check")
 */
class Check extends Xml implements RequestInterface
{
    use Parameters;

    public function getRuntimeParams()
    {
        $params = [
            'payment_gate_name' => $this->getSource()->getName(),
            'service_name' => $this->getDestination()->getName(),
        ];
        $runtimeClass = 'workflow\runtime\TransactionParametersWorkflowRuntime';

        return [$runtimeClass, $params];
    }

    /**
     * @return Destination
     */
    public function getDestination()
    {
        return $this->destination;
    }

    /**
     * @return Source
     */
    public function getSource()
    {
        return $this->source;
    }
    /**
     * @NotBlank
     * @Type("N1\Xml\Request\CheckCreate\Destination")
     */
    protected $destination;

    /**
     * @NotBlank
     * @Type("N1\Xml\Request\CheckCreate\Source")
     */
    protected $source;

    public function __construct(Destination $destination, Source $source, $parameters = [])
    {
        $this->destination = $destination;
        $this->source = $source;
        $this->setParameters($parameters);
    }
}
