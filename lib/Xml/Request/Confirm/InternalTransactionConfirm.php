<?php

namespace N1\Xml\Request\Confirm;

use JMS\Serializer\Annotation\XmlRoot;
use N1\Xml\Request\ConfirmStatus\IInternalTransaction;

/**
 * @XmlRoot("confirm")
 */
class InternalTransactionConfirm extends Confirm implements IInternalTransaction
{
    use \N1\Xml\Request\Traits\InternalTransaction;

    public function __construct($transactionId, $parameters = [])
    {
        $this->transaction_id = $transactionId;
        $this->setParameters($parameters);
    }
}
