<?php

namespace N1\Xml\Request\Confirm;

use N1\Xml\Request\Status\Status;

abstract class Confirm extends Status
{
    use \N1\Xml\Amount\AmountTrait;
}
