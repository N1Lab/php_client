<?php

namespace N1\Xml\Request\Confirm;

use JMS\Serializer\Annotation\XmlRoot;
use N1\Xml\Request\ConfirmStatus\IExternalTransaction;

/**
 * @XmlRoot("confirm")
 */

class ExternalTransactionConfirm extends Confirm implements IExternalTransaction
{
    use \N1\Xml\Request\Traits\ExternalTransaction;

    public function __construct($transaction, $parameters = [])
    {
        $this->extTransaction = $transaction;
        $this->setParameters($parameters);
    }
}
