<?php

namespace N1\Xml\Request\Traits;

use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

trait ExternalTransaction
{
    public function getRuntimeParams()
    {
        $params = [
            'ext_transaction_id' => $this->getExtTransaction(),
        ];
        $runtimeClass = 'workflow\runtime\ExtTransactionWorkflowRuntime';

        return [$runtimeClass, $params];
    }

    /**
     * @Assert\NotBlank
     * @Type("string")
     * @SerializedName("extTransaction")
     */
    protected $extTransaction;

    /**
     * @param mixed $extTransaction
     * @return $this
     */
    public function setExtTransaction($extTransaction)
    {
        $this->extTransaction = $extTransaction;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getExtTransaction()
    {
        return $this->extTransaction;
    }
}
