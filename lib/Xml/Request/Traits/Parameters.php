<?php

namespace N1\Xml\Request\Traits;

use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\XmlMap;
use JMS\Serializer\Annotation\XmlElement;
use JMS\Serializer\Annotation\SerializedName;

trait Parameters
{
    /**
     * @return ArrayCollection
     */
    public function getParameters()
    {
        return $this->parameters;
    }

    /**
     * @XmlElement(cdata=false)
     * @SerializedName("parameters")
     * @Type("ArrayCollection<string, string>")
     * @XmlMap(keyAttribute = "name", entry="parameter")
     */
    protected $parameters;

    /**
     * @param mixed $parameters
     * @throws \Exception
     * @return $this
     */
    public function setParameters($parameters)
    {
        if (is_array($parameters)) {
            $this->parameters = new ArrayCollection($parameters);
        } elseif ($parameters instanceof ArrayCollection) {
            $this->parameters = $parameters;
        } else {
            throw new \Exception('Parameters must be array or ArrayCollection');
        }

        return $this;
    }
}
