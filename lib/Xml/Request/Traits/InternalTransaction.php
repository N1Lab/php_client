<?php

namespace N1\Xml\Request\Traits;

use JMS\Serializer\Annotation\Type;
use N1\Xml\Annotation\NotBlank;

trait InternalTransaction
{
    public function getRuntimeParams()
    {
        $params = [
            'transaction_id' => $this->getTransactionId(),
        ];
        $runtimeClass = 'workflow\runtime\TransactionWorkflowRuntime';

        return [$runtimeClass, $params];
    }

    /**
     * @NotBlank
     * @Type("integer")
     */
    protected $transaction_id;

    /**
     * @param mixed $transaction
     * @return $this
     */
    public function setTransactionId($transaction)
    {
        $this->transaction_id = $transaction;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTransactionId()
    {
        return $this->transaction_id;
    }
}
