<?php

namespace N1\Xml\Request\Pay;

use JMS\Serializer\Annotation\XmlRoot;
use N1\Xml\Request\Create\Create;
use Symfony\Component\Validator\Constraints as Assert;

/** @XmlRoot("pay") */
class Pay extends Create
{
}
