<?php

namespace N1\Xml\Request\ConfirmStatus;

interface IExternalTransaction
{
    public function setExtTransaction($extTransaction);

    public function getExtTransaction();
}
