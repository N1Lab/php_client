<?php

namespace N1\Xml\Request\ConfirmStatus;

interface IInternalTransaction
{
    public function setTransactionId($transactionId);

    public function getTransactionId();
}
