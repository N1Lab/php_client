<?php

namespace N1\Xml\Request\CheckCreate;

use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\XmlElement;
use N1\Xml\Annotation\NotBlank;
use N1\Xml\Xml;

class Source extends Xml
{
    /**
     * @NotBlank
     * @XmlElement(cdata=false)
     * @Type("string")
     */
    private $name;

    public function __construct($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }
}
