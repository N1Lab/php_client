<?php

namespace N1\Xml\Request\CheckCreate;

use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\XmlRoot;
use JMS\Serializer\Annotation\XmlElement;
use N1\Xml\Annotation\NotBlank;
use N1\Xml\Xml;

/** @XmlRoot("destination") */
class Destination extends Xml
{
    /**
     * @NotBlank
     * @XmlElement(cdata=false)
     * @Type("string")
     */
    private $account;

    /**
     * @NotBlank
     * @XmlElement(cdata=false)
     * @Type("string")
     */
    private $name;
    
    /**
     * @return mixed
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    public function __construct($account, $name)
    {
        $this->account = $account;
        $this->name = $name;
    }
}
