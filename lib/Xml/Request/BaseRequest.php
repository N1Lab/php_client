<?php

namespace N1\Xml\Request;

use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\XmlElement;
use N1\Xml\Annotation\NotBlank;

class BaseRequest
{
    /**
     * @NotBlank
     * @XmlElement(cdata=false)
     * @Type("DateTime<'Y-m-d H:i:s', 'Europe/Kiev'>")
     */
    protected $date;

    /**
     * @Type("string")
     * @XmlElement(cdata=false)
     */
    public $sign;

    public function __construct()
    {
        $this->date = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param string $date
     * @return $this
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }
}
