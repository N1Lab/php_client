<?php

namespace N1\Xml\Request;

use JMS\Serializer\Annotation\Exclude;
use JMS\Serializer\Annotation\HandlerCallback;
use JMS\Serializer\Annotation\XmlRoot;
use JMS\Serializer\Context;
use JMS\Serializer\Metadata\PropertyMetadata;
use JMS\Serializer\XmlDeserializationVisitor;
use JMS\Serializer\XmlSerializationVisitor;
use N1\Errors\ApiErrorCodes;
use N1\Errors\ApiException;
use N1\Xml\Request\Check\Check;
use N1\Xml\Request\Confirm\Confirm;
use N1\Xml\Request\Create\Create;
use N1\Xml\Request\Pay\Pay;
use N1\Xml\Request\Status\Status;
use N1\Xml\Request\CoreReceiver\Notify;

/**
 * @XmlRoot("request")
 */
class Request extends BaseRequest
{
    /**
     * для sandbox
     *
     * @Exclude
     */
    public $return_service_response = false;

    /**
     * @Exclude
     */
    private $type;

    /**
     * Возвращает тип запроса(check, create, confirm или status)
     *
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Данные запроса из корневого тега (<check></check>)
     *
     * @var RequestInterface
     */
    private $data;

    /**
     * @return RequestInterface
     */
    public function getData()
    {
        return $this->data;
    }

    public function __construct(RequestInterface $type)
    {
        parent::__construct();

        $this->data = $type;

        if ($type instanceof Pay) {
            $this->pay = $type;
            $this->type = 'pay';
        } elseif ($type instanceof Create) {
            $this->create = $type;
            $this->type = 'create';
        } elseif ($type instanceof Check) {
            $this->check = $type;
            $this->type = 'check';
        } elseif ($type instanceof Confirm) {
            $this->type = 'confirm';
        } elseif ($type instanceof Status) {
            $this->type = 'status';
        } elseif ($type instanceof Notify) {
            $this->type = 'notify';
//        } elseif ($type instanceof Report) {
//            $this->type = 'report';
        } else {
            throw new ApiException(ApiErrorCodes::INVALID_REQUEST);
        }
    }

    /**
     * @HandlerCallback("xml", direction = "serialization")
     */
    public function serializeToXml(XmlSerializationVisitor $visitor, $data, Context $context)
    {
        $metadata = $context->getMetadataFactory()->getMetadataForClass('N1\Xml\Request\Request');
        $type = $this->resolveSerializationType();
        $metadata->addPropertyMetadata($this->createDataProperty($type, $this->resolveSerializationName($type, $context)));

        $visitor->startVisitingObject($metadata, $this, ['name' => 'N1\Xml\Request\Request', 'params' => []], $context);

        foreach ($metadata->propertyMetadata as $propertyMetadata) {
            $visitor->visitProperty($propertyMetadata, $this, $context);
        }
    }

    /** @HandlerCallback("xml", direction = "deserialization") */
    public function deserializeFromXml(XmlDeserializationVisitor $visitor, \SimpleXMLElement $data, Context $context)
    {
        $metadata = $context->getMetadataFactory()->getMetadataForClass('N1\Xml\Request\Request');

        $type = $this->resolveDeserializationType($data);
        $name = $this->resolveSerializationName($type, $context);

        $this->type = $name;

        $metadata->addPropertyMetadata($this->createDataProperty($type, $name));

        $visitor->startVisitingObject($metadata, $this, ['name' => 'N1\Xml\Request\Request', 'params' => []], $context);

        foreach ($metadata->propertyMetadata as $propertyMetadata) {
            $context->pushPropertyMetadata($propertyMetadata);
            $visitor->visitProperty($propertyMetadata, $data, $context);
            $context->popPropertyMetadata();
        }

        $this->checkDate();
    }

    private function createDataProperty($type, $name)
    {
        $metadataProperty = new PropertyMetadata('N1\Xml\Request\Request', 'data');
        $metadataProperty->setType($type);
        $metadataProperty->serializedName = $name;

        return $metadataProperty;
    }

    private function resolveSerializationType()
    {
        return get_class($this->data);
    }

    private function resolveSerializationName($type, Context $context)
    {
        /** @var \JMS\Serializer\Metadata\ClassMetadata $metadata */
        $metadata = $context->getMetadataFactory()->getMetadataForClass($type);

        return $metadata->xmlRootName;
    }

    private function resolveDeserializationType(\SimpleXMLElement $data)
    {
		if (isset($data->status->transaction_id)) {
            return 'N1\Xml\Request\Status\InternalTransactionStatus';
		} elseif (isset($data->status->destination->name) && isset($data->status->destination->account) && isset($data->status->extTransaction)) {
			return 'N1\Xml\Request\Status\ExternalTransactionStatusByOrder';
	    } elseif (isset($data->status->extTransaction)) {
			return 'N1\Xml\Request\Status\ExternalTransactionStatus';
		}elseif (isset($data->confirm->transaction_id)) {
            return 'N1\Xml\Request\Confirm\InternalTransactionConfirm';
        } elseif (isset($data->confirm->extTransaction)) {
            return 'N1\Xml\Request\Confirm\ExternalTransactionConfirm';
        } elseif (isset($data->check)) {
            return 'N1\Xml\Request\Check\Check';
        } elseif (isset($data->create)) {
            return 'N1\Xml\Request\Create\Create';
        } elseif (isset($data->pay)) {
            return 'N1\Xml\Request\Pay\Pay';
        } elseif (isset($data->report)) {
            return 'N1\Xml\Request\Report\Report';
        }

        throw new ApiException(ApiErrorCodes::INVALID_REQUEST, "Can't resolve request type");
    }

    private function checkDate()
    {
        $date_now_min = (new \DateTime())->add(new \DateInterval('P1D'));
        $date_now_max = (new \DateTime())->sub(new \DateInterval('P1D'));

        if ($this->date > $date_now_min || $this->date < $date_now_max) {
            throw new ApiException(ApiErrorCodes::INVALID_REQUEST, 'Datetime not in range');
        }
    }
}
