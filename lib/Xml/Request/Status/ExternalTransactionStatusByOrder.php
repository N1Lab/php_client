<?php

namespace N1\Xml\Request\Status;

use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\XmlRoot;
use N1\Xml\Annotation\NotBlank;
use N1\Xml\Request\ConfirmStatus\IExternalTransaction;
use N1\Xml\Request\CheckCreate\Destination;

/**
 * @XmlRoot("status")
 */
class ExternalTransactionStatusByOrder extends Status implements IExternalTransaction
{
    use \N1\Xml\Request\Traits\ExternalTransaction;

	/**
	 * @NotBlank
	 * @Type("N1\Xml\Request\CheckCreate\Destination")
	 */
	protected $destination;

	public function getRuntimeParams()
	{
		$params = [
			'ext_transaction_id' => $this->getExtTransaction(),
			'service_account' => $this->destination->getAccount(),
			'service_name' => $this->destination->getName(),
		];
		$runtimeClass = 'workflow\runtime\ExtTransactionByOrderWorkflowRuntime';

		return [$runtimeClass, $params];
	}

    public function __construct($extTransaction, \N1\Xml\Request\CheckCreate\Destination $destination)
    {
	    $this->destination = $destination;
        $this->setExtTransaction($extTransaction);
        $this->type = 'external';
    }
}
