<?php

namespace N1\Xml\Request\Status;

use N1\Xml\Request\RequestInterface;
use N1\Xml\Request\Traits\Parameters;
use Symfony\Component\Validator\Constraints as Assert;
use N1\Xml\Xml;

abstract class Status extends Xml implements RequestInterface
{
    use Parameters;
}
