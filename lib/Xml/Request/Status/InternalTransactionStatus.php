<?php

namespace N1\Xml\Request\Status;

use JMS\Serializer\Annotation\XmlRoot;
use N1\Xml\Request\ConfirmStatus\IInternalTransaction;

/**
 * @XmlRoot("status")
 */
class InternalTransactionStatus extends Status implements IInternalTransaction
{
    use \N1\Xml\Request\Traits\InternalTransaction;

    public function __construct($transactionId)
    {
        $this->transaction_id = $transactionId;
    }
}
