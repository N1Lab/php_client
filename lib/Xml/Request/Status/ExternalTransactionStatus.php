<?php

namespace N1\Xml\Request\Status;

use JMS\Serializer\Annotation\XmlRoot;
use N1\Xml\Request\ConfirmStatus\IExternalTransaction;

/**
 * @XmlRoot("status")
 */
class ExternalTransactionStatus extends Status implements IExternalTransaction
{
    use \N1\Xml\Request\Traits\ExternalTransaction;

    public function __construct($extTransaction)
    {
        $this->extTransaction = $extTransaction;
        $this->type = 'external';
    }
}
