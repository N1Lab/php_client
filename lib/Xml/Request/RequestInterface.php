<?php

namespace N1\Xml\Request;

use Doctrine\Common\Collections\ArrayCollection;

interface RequestInterface
{
    public function getRuntimeParams();
    /** @return ArrayCollection */
    public function getParameters();
}