<?php

namespace N1\Xml\Request\Create;

use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\XmlElement;
use JMS\Serializer\Annotation\XmlRoot;
use N1\Xml\Annotation\NotBlank;
use N1\Xml\Request\Check\Check;
use N1\Xml\Request\CheckCreate\Destination;
use N1\Xml\Request\CheckCreate\Source;
use Symfony\Component\Validator\Constraints as Assert;


/** @XmlRoot("create") */
class Create extends Check
{
    /**
     * @NotBlank
     * @Assert\Type(type="double", message="The value {{ value }} is not a valid {{ type }}.")
     * @Assert\GreaterThan(
     *     value = 0, message="Amount should be greater than {{ compared_value }}"
     * )
     * @Assert\LessThan(
     *     value = 1000000000000, message="Amount should be less than {{ compared_value }}"
     * )
     * @Type("double")
     */
    private $amount;

    /**
     * @return mixed
     */
    public function getExternalDate()
    {
        return $this->external_date;
    }

    /**
     * @return mixed
     */
    public function getExternalId()
    {
        return $this->ext_transaction_id;
    }

    /**
     * @Assert\Length(
     *      min = 1,
     *      max = 256,
     *      minMessage = "Transaction id must be at least {{ limit }} characters long",
     *      maxMessage = "Transaction id cannot be longer than {{ limit }} characters long"
     * )
     * @Type("string")
     * @XmlElement(cdata=false)
     */
    private $ext_transaction_id;

    /**
     * @Type("string")
     */
    private $external_date;

    /**
     * @return double
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $external_date
     * @return $this
     */
    public function setExternalDate($external_date)
    {
        $this->external_date = $external_date;

        return $this;
    }

    /**
     * @param mixed $transaction_id
     * @return $this
     */
    public function setExternalId($transaction_id)
    {
        $this->ext_transaction_id = $transaction_id;

        return $this;
    }

    public function __construct($amount, Destination $destination, Source $source, $parameters = [])
    {
        $this->amount = $amount;
        $this->destination = $destination;
        $this->source = $source;
        $this->date = new \DateTime();
        $this->setParameters($parameters);
    }
}
