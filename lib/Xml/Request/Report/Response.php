<?php

namespace N1\Xml\Request\Report;

use N1\Xml\Xml;
use JMS\Serializer\Annotation\XmlRoot;
use JMS\Serializer\Annotation\Type;
use N1\Xml\Annotation\NotBlank;
use Symfony\Component\Validator\Constraints;

/** @XmlRoot("response") */
class Response extends Xml implements IReport
{
    /**
     * @NotBlank
     * @Type("integer")
     * @Constraints\GreaterThan(value = 0, message="Invalid Id")
     */
    public $id;

    /**
     * @Type("integer")
     * @Constraints\GreaterThan(value = 0, message="Invalid Seek")
     */
    public $seek;

    /**
     * @Type("integer")
     * @Constraints\GreaterThan(value = 0, message="Invalid Size")
     */
    public $size;

    public function getId()
    {
        return $this->id;
    }
}
