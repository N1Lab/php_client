<?php

namespace N1\Xml\Request\Report;

use N1\Xml\Xml;
use JMS\Serializer\Annotation\XmlRoot;
use JMS\Serializer\Annotation\Type;
use N1\Xml\Annotation\NotBlank;
use Symfony\Component\Validator\Constraints;

/** @XmlRoot("create") */
class Create extends Xml implements IReport
{
    const DEF_FORMAT = "Y-m-d";

    /**
     * @NotBlank
     * @Type("string")
     * @Constraints\Date(message = "Invalid 'from' attribute, expected format Y-m-d")
     */
    public $from;

    /**
     * @NotBlank
     * @Type("string")
     * @Constraints\Date(message = "Invalid 'to' attribute, expected format Y-m-d")
     */
    public $to;

    /**
     * @return mixed
     */
    public function getFrom()
    {
        $dateTime = new \DateTime();
        if (!$this->from) {
            $this->from = $dateTime->format(self::DEF_FORMAT);
        }

        return $this->from;
    }

    /**
     * @return mixed
     */
    public function getTo()
    {
        $dateTime = new \DateTime();
        if (!$this->to) {
            $this->to = $dateTime->format(self::DEF_FORMAT);
        }
        return $this->to;
    }

    public function getId()
    {
        return null;
    }
}
