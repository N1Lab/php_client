<?php

namespace N1\Xml\Request\Report;

use N1\Errors\ApiErrorCodes;
use N1\Errors\ApiException;
use N1\Xml\Xml;
use JMS\Serializer\Annotation\XmlRoot;
use JMS\Serializer\Annotation\Type;

/** @XmlRoot("report") */
class Report extends Xml implements IReport
{
    /**
     * @var IReport
     * @Type("N1\Xml\Request\Report\Create")
     */
    public $create;

    /**
     * @var IReport
     * @Type("N1\Xml\Request\Report\Response")
     */
    public $response;

    /**
     * @var IReport
     * @Type("N1\Xml\Request\Report\Status")
     */
    public $status;

    /**
     * @return string
     * @throws ApiException
     */
    public function getAction()
    {
        switch (true) {
            case isset($this->create):
                return 'create';
            case isset($this->response):
                return 'response';
            case isset($this->status):
                return 'status';
            default:
                throw new ApiException(ApiErrorCodes::INVALID_REQUEST, 'empty request');
        }
    }

    /**
     * @return IReport
     * @throws ApiException
     */
    public function getData()
    {
        switch (true) {
            case $this->create:
                return $this->create;
            case $this->response:
                return $this->response;
            case $this->status:
                return $this->status;
            default:
                throw new ApiException(ApiErrorCodes::INVALID_REQUEST, 'empty data');
        }
    }

    /**
     * @return mixed
     * @throws ApiException
     */
    public function getId()
    {
        return $this->getData()->getId();
    }
}
