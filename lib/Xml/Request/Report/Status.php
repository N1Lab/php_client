<?php

namespace N1\Xml\Request\Report;

use N1\Xml\Xml;
use JMS\Serializer\Annotation\XmlRoot;
use JMS\Serializer\Annotation\Type;
use N1\Xml\Annotation\NotBlank;
use Symfony\Component\Validator\Constraints;

/** @XmlRoot("status") */
class Status extends Xml implements IReport
{
    /**
     * @NotBlank
     * @Type("integer")
     * @Constraints\GreaterThan(value = 0, message="Invalid Id")
     */
    public $id;

    public function getId()
    {
        return $this->id;
    }
}
