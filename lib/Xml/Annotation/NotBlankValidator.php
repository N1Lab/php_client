<?php

namespace N1\Xml\Annotation;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\NotBlankValidator as SymfonyNotBlankValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class NotBlankValidator extends SymfonyNotBlankValidator
{
    /**
     * {@inheritdoc}
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof NotBlank) {
            throw new UnexpectedTypeException($constraint, __NAMESPACE__.'\NotBlank');
        }

        if (false === $value || (empty($value) && '0' != $value)) {
            $this->context->addViolation($constraint->message, array(
                '{{ value }}' => $this->context->getPropertyPath(),
            ));
        }
    }
}
