<?php

namespace N1\Widget;

use N1\Enum\PaymentGateTypes;
use N1\Errors\ApiErrorCodes;
use N1\Errors\ApiException;

class LinkGenerator
{
    const DATE_FORMAT = 'Y-m-d H:i:s';
    
    protected $widgetHostUrl;
    
    protected $signature;
    
    public function __construct(Signature $signature, $widgetHostUrl)
    {
        $this->signature = $signature;
        $this->widgetHostUrl = $widgetHostUrl;
    }
    
    public function generatePaymentShowCaseLink(
        $serviceTechnicalName,
        $amount,
        $orderId,
        $comment = null,
        $account = null,
        $phone = null,
        $email = null,
        $paymentDateTime = null
    ) {
        return $this->buildLink(
            $paymentDateTime,
            null,
            $serviceTechnicalName,
            $amount,
            $orderId,
            $paymentDateTime,
            $comment,
            $account,
            $phone,
            $email
        );
    }

    public function generatePaymentTerminalLink(
        $paymentGateTypeName,
        $serviceTechnicalName,
        $amount,
        $orderId,
        $comment = null,
        $account = null,
        $phone = null,
        $email = null,
        $paymentDateTime = null
    ) {
        if (!in_array($paymentGateTypeName, PaymentGateTypes::getAvailableWidgetPaymentGateTypes())) {
            throw new ApiException(ApiErrorCodes::INVALID_REQUEST, 'Payment gate type incorrect');
        }
        
        return $this->buildLink(
            $paymentDateTime,
            $paymentGateTypeName,
            $serviceTechnicalName,
            $amount,
            $orderId,
            $paymentDateTime,
            $comment,
            $account,
            $phone,
            $email
        );
    }
    
    protected function buildLink(
        $paymentDateTime,
        $paymentGateTypeName,
        $serviceTechnicalName,
        $amount,
        $orderId,
        $paymentDateTime,
        $comment,
        $account,
        $phone,
        $email
    ) {
        $paymentDateTime = (empty($paymentDateTime)) ? date(self::DATE_FORMAT) : $paymentDateTime;

        $params = [
            'service_name' => $serviceTechnicalName,
            'amount' => $amount,
            'order_id' => $orderId,
            'payment_date_time' => $paymentDateTime,
        ];

        if (!empty($paymentGateTypeName)) {
            $params['payment_gate_type_name'] = $paymentGateTypeName;
        }
        
        if (!empty($comment)) {
            $params['comment'] = $comment;
        }

        if (!empty($account)) {
            $params['account'] = $account;
        }

        if (!empty($phone)) {
            $params['phone'] = $phone;
        }
        if (!empty($email)) {
            $params['email'] = $email;
        }

        $params[Signature::SIGN_TAG_NAME] = $this->signature->signParams($params);

        $query = http_build_query($params);

        return $this->widgetHostUrl . '?' . $query;
    }
}
