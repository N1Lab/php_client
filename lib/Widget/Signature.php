<?php

namespace N1\Widget;

class Signature
{
    const SIGN_TAG_NAME = 'signature';
    
    protected $secretKey;

    public function __construct($secretKey)
    {
        $this->secretKey = $secretKey;
    }
    
    public function checkSign($sign, $checkParams)
    {
        ksort($checkParams);

        $signatureExpected = '';

        foreach ($checkParams as $key => $value) {
            $signatureExpected .= $key.$value;
        }

        $signatureExpected .= $this->secretKey;

        if (md5($signatureExpected) == $sign) {
            return true;
        }

        return false;
    }

    public function signParams($params)
    {
        ksort($params);

        $signatureExpected = '';

        foreach ($params as $key => $value) {
            $signatureExpected .= $key.$value;
        }

        $signatureExpected .= $this->secretKey;

        return md5($signatureExpected);
    }
}
