<?php

namespace N1\Enum;

class PaymentGateTypes
{
    const IMPORT = 'import';
    const TERMINAL = 'terminal';
    const MOBILE_COMMERCE = 'mobile commerce';
    const CREDIT_CARD = 'credit card';
    const N1_BUTTON = 'n1_button';
    const E_MONEY = 'e_money';

    public static function getAvailableWidgetPaymentGateTypes()
    {
        return [
            self::MOBILE_COMMERCE, self::CREDIT_CARD, self::N1_BUTTON, self::E_MONEY
        ];
    }
}
