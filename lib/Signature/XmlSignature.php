<?php

namespace N1\Signature;

use N1\Errors\IncorrectSignatureException;

class XmlSignature
{
    const SIGN_TAG_NAME = 'sign';

    private $privateKey;
    private $publicKey;
    private $tag;
    private $enabled;
    private $signature;
    
    public function __construct($config)
    {
        $this->privateKey = $config['privateKey'];
        $this->publicKey = $config['publicKey'];
        $this->tag = !empty($config['tag']) ? $config['tag'] : self::SIGN_TAG_NAME;
        $this->enabled = isset($config['enabled']) ? $config['enabled'] : true;
        $this->signature = new Signature();
    }

    /**
     * @return string
     */
    public function isEnabled()
    {
        return $this->enabled;
    }

    /**
     * @return mixed
     */
    public function getTagName()
    {
        return $this->tag;
    }

    /**
     * @param $xmlData
     * @return bool
     * @throws IncorrectSignatureException
     * @throws \Exception
     */
    public function checkSign($xmlData)
    {
        if (!$this->publicKey) {
            throw new \Exception('Public key is empty');
        }
        
        if (!is_string($xmlData) or empty($xmlData)) {
            throw new IncorrectSignatureException('Check data is empty');
        }

        if (
            preg_match(str_replace(':sign_tag', $this->tag, '#<:sign_tag>(.*)</:sign_tag>#'), $xmlData, $matches) == 0
        ) {
            throw new IncorrectSignatureException('Sign tag not found');
        }

        $sign = $matches[1];

        $xmlData = str_replace($sign, '', $xmlData);

        try {
            if ($this->signature->check($this->publicKey, $xmlData, $sign)) {
                return true;
            }
        } catch (\Exception $e) {
            throw new IncorrectSignatureException($e->getMessage());
        }

        throw new IncorrectSignatureException('Sign check failed');
    }

    /**
     * @param $xmlData
     * @throws \Exception
     * @return string
     */
    public function signData($xmlData)
    {
        if (!$this->privateKey) {
            throw new \Exception('Private key is empty');
        }
        
        if (!strstr($xmlData, str_replace(':sign_tag', $this->tag, '<:sign_tag></:sign_tag>'))) {
            throw new \Exception(sprintf('Empty <%s> tag not found', $this->tag));
        }

        $hexSign = strtoupper($this->signature->sign($this->privateKey, $xmlData));
        
        $search = str_replace(':sign_tag', $this->tag, '#<:sign_tag>(.*)</:sign_tag>#');
        $replace = str_replace(':sign_tag', $this->tag, "<:sign_tag>$hexSign</:sign_tag>");

        return preg_replace($search, $replace, $xmlData);
    }
}
