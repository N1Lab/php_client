<?php

namespace N1\Signature;

class Signature
{
    const ALGORITHM = OPENSSL_ALGO_SHA1;

    public function check($publicKey, $data, $sign)
    {
        if (!is_string($data) or empty($data)) {
            throw new \Exception('Check data is empty');
        }
        
        $publicKey = openssl_pkey_get_public($publicKey);

        if ($publicKey == false) {
            throw new \Exception("Can't load public key");
        }

        if (!ctype_xdigit($sign)) {
            throw new \Exception('Invalid sign value. Must be hexadecimal');
        }

        $res = openssl_verify(
            $data,
            pack("H*", $sign),
            $publicKey,
            self::ALGORITHM
        );

        openssl_free_key($publicKey);

        if ($res !== 1) {
            return false;
        }
        
        return true;
    }
    
    public function sign($privateKey, $data)
    {
        $privateKey = openssl_pkey_get_private($privateKey);

        if (!$privateKey) {
            throw new \Exception("Can't load private key");
        }

        openssl_sign(
            $data,
            $sign,
            $privateKey,
            self::ALGORITHM
        );

        $hexSign = bin2hex($sign);
        openssl_free_key($privateKey);
        
        return $hexSign;
    }
}
