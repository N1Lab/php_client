<?php

namespace N1\Signature;

class LinkSignature
{
    const SIGNATURE_PARAMETER_NAME = 'signature';
    
    private $signature;

    public function __construct(Signature $signature)
    {
        $this->signature = $signature;
    }

    protected function generateSignatureValue($parameters = [])
    {
        ksort($parameters);
        
        $signValue = '';

        foreach ($parameters as $name => $value) {
            $signValue .= $name . $value;
        }

        return $signValue;
    }
    
    public function signGetParameters($privateKey, $parameters = [])
    {
        return $this->signature->sign($privateKey, $this->generateSignatureValue($parameters));
    }
    
    public function checkGetParameters($publicKey, $parameters, $signature)
    {
        return $this->signature->check($publicKey, $this->generateSignatureValue($parameters), $signature);
    }
}
