<?php

namespace N1\Errors;

class ApiErrorCodes
{
    const INCORRECT_SIGNATURE = 1;

    const SYSTEM_ERROR = 2;

    const INVALID_REQUEST = 3;

    const CARD_OR_ACCOUNT_IS_EXPIRED = 4;

    const CARD_OR_ACCOUNT_IS_BLOCKED = 5;

    const INSUFFICIENT_FUNDS = 6;

    const INCORRECT_SUM = 7;

    const TRANSACTION_CANCELED = 8;

    const TRANSACTION_NOT_FOUND = 9;

    const TRANSACTION_DUPLICATED = 10;

    const OPERATION_PROHIBITED = 11;

    const SERVICE_UNAVAILABLE = 12;

    const COUNTERPARTY_ERROR = 13;

    const TRANSACTION_LOCKED = 14;

    const RECALL_PROHIBITED = 15;

    const UNABAILABLE_PAYMENT_GATE = 16;

    const NOT_POSTPAID_PAYMENT = 131;
    
    const INVALID_CONFIRMATION_CODE = 132;

    public static $defaultDescriptions = [
        1 => 'Incorrect signature',
        2 => 'System error',
        3 => 'Invalid request',
        4 => 'Card/account is expired',
        5 => 'Card/account is blocked',
        6 => 'Insufficient funds in the account',
        7 => 'Incorrect sum',
        8 => 'Transaction canceled',
        9 => 'Transaction not found',
        10 => 'Transaction with this Id already exists',
        11 => 'The operation is prohibited for current counterparty',
        12 => 'Service unavailable',
        13 => 'Counterparty error',
        14 => 'Transaction is locked',
        15 => 'Recall operation is prohibited',
        self::UNABAILABLE_PAYMENT_GATE => 'Payment gate is unavailable',
        self::NOT_POSTPAID_PAYMENT => 'The payment is not possible for postpaid subscriber',
        self::INVALID_CONFIRMATION_CODE => 'Invalid confirmation code',
    ];
}
