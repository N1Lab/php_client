<?php

namespace N1\Errors;

use Doctrine\Common\Collections\ArrayCollection;

class ApiException extends \Exception
{
    private $mappingExceptions = [
        ApiErrorCodes::SERVICE_UNAVAILABLE => ServiceUnavailableException::class,
        ApiErrorCodes::INCORRECT_SIGNATURE => IncorrectSignatureException::class,
    ];
    
    private $parameters = [];

    /**
     * @return ArrayCollection
     */
    public function getParameters()
    {
        return $this->parameters;
    }

    /**
     * @param array $parameters
     */
    public function setParameters($parameters)
    {
        if ($parameters instanceof ArrayCollection) {
            $this->parameters = $parameters;
        } else {
            $this->parameters = new ArrayCollection($parameters);
        }
    }

    public function __construct($code, $message = '', $parameters = [], \Exception $previous = null)
    {
        if (get_class($this) == ApiException::class && array_key_exists($code, $this->mappingExceptions)) {
            throw new $this->mappingExceptions[$code]($message);    
        }
        
        if (!is_int($code) or !isset(ApiErrorCodes::$defaultDescriptions[$code])) {
            throw new \Exception('Error code not valid');
        }

        if (empty($message)) {
            $message = ApiErrorCodes::$defaultDescriptions[$code];
        }

        $this->setParameters($parameters);

        parent::__construct($message, $code, $previous);
    }
}
