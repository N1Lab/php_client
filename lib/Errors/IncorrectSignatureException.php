<?php

namespace N1\Errors;

class IncorrectSignatureException extends ApiException
{
    public function __construct($message = '')
    {
        parent::__construct(ApiErrorCodes::INCORRECT_SIGNATURE, $message);
    }
}
