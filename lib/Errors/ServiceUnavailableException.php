<?php

namespace N1\Errors;

class ServiceUnavailableException extends ApiException
{
    public function __construct($message = '')
    {
        parent::__construct(ApiErrorCodes::SERVICE_UNAVAILABLE, $message);
    }
}
